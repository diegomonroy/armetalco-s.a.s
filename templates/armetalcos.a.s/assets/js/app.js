// JavaScript Document

/* ************************************************************************************************************************

ARMETALCO S.A.S

File:			app.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2017

************************************************************************************************************************ */

/* Foundation */

$(document).foundation();

/* WOW */

new WOW().init();

/* jQuery */

jQuery.noConflict();

jQuery(document).ready(function () {

	/* Foundation */

	jQuery( '#nombre, #correo, #telefono, #ciudad, #asunto, #acepto, #mensaje' ).foundation( 'destroy' );

});